# Basic periodic functions

## Additional shaders
Load ![lib-wave.js](./../../lib/lib-wave.js)
```javascript
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/lib-wave.js")
```

## Examples
You can find examples made with `lib-wave` in this folder: [examples/wave](./../../examples/wave).

## Description
These additional shaders generate simple periodic wave patterns. They can be used in all places where `osc()` can be used. All these patterns are of type `src`.

All these shaders provide gray-scale or black and white output. They are designed so that the output is approximately in the range 0 to 1.

At first glance, these patterns are boring. They are, when standing alone, too repetitive and too predictable. It gets interesting when you combine these shaders in feedback loops. An example can be found here: [examples/wave/demo-wave-feedback.js](./../../examples/wave/demo-wave-feedback.js)

### Unsigned Sine Wave

Like a sine function, but the output is in the range [0.0 .. 1.0].

`usin( frequency, speed )`

* `frequency` :: float (default `10`)
* `speed` :: float (default `0.1`)

![](./usin.png)

### Harmonic Wave

Adding 3 sine waves with descending amplitudes and increasing frequencies.
The harmonics can be phase shifted.
The output is approximately in the range [0.0 .. 1.0].

`harmonic( frequency, speed, phase1, phase2)`

* `frequency` :: float (default `10`)
* `speed` :: float (default `0.1`)
* `phase1` :: float (default `0`)
* `phase2` :: float (default `0`)

```javascript
harmonic(10,
         0.1,
         ()=> { return 0.05*Math.sin(0.5*time); },
         ()=> { return 0.10*Math.sin(time); })
.out(o0)
```

![](./harmonic.png)

### Square Wave

A square wave with hard edges.

`squ( frequency, speed )`

* `frequency` :: float (default `10`)
* `speed` :: float (default `0.1`)

![](./squ.png)

### Triangle Wave

A triangle wave.

`tri( frequency, speed )`

* `frequency` :: float (default `10`)
* `speed` :: float (default `0.1`)

![](./tri.png)

### Sawtooth Wave

A sawtooth wave.

`saw( frequency, speed )`

* `frequency` :: float (default `10`)
* `speed` :: float (default `0.1`)

![](./saw.png)

### Radial unsigned Sine Wave

A circularly propagating sine wave. The output is in the range [0.0 .. 1.0].

`rusin( frequency, speed )`

* `frequency` :: float (default `10`)
* `speed` :: float (default `0.1`)

![](./rusin.jpg)

### Radial Harmonic Wave

Circular propagating sine waves.
Adding 3 sine waves with descending amplitudes and increasing frequencies.
The harmonics can be phase shifted.
The output is approximately in the range [0.0 .. 1.0].

`rharmonic( frequency, speed, phase1, phase2)`

* `frequency` :: float (default `10`)
* `speed` :: float (default `0.1`)
* `phase1` :: float (default `0`)
* `phase2` :: float (default `0`)

```javascript
rharmonic(10,
         0.1,
         ()=> { return 0.05*Math.sin(0.5*time); },
         ()=> { return 0.10*Math.sin(time); })
.out(o0)
```

![](./rharmonic.jpg)

### Radial Square Wave

A circular propagating square wave with hard edges.

`rsqu( frequency, speed )`

* `frequency` :: float (default `10`)
* `speed` :: float (default `0.1`)

![](./rsqu.png)

### Radial Triangle Wave

A circular propagating triangle wave.

`rtri( frequency, speed )`

* `frequency` :: float (default `10`)
* `speed` :: float (default `0.1`)

![](./rtri.jpg)

### Radial Sawtooth Wave

A circular propagating sawtooth wave.

`rsaw( frequency, speed )`

* `frequency` :: float (default `10`)
* `speed` :: float (default `0.1`)

![](./rsaw.jpg)

### Vertical stripes

Gray - scale stripes along the x axis.

`stripes( frequency, speed )`

* `frequency` :: float (default `10`)
* `speed` :: float (default `1`)

![](./stripes.png)

### Concentric stripes

Concentric gray - scale stripes.

`rstripes( frequency, speed )`

* `frequency` :: float (default `10`)
* `speed` :: float (default `1`)

![](./rstripes.png)
