# Soft patterns
These shaders generate patterns with smooth transitions. Look and feel are the opposite of [lib-pattern](./../pattern/README.md). We have here soft borders instead of hard edges.

In terms of behavior they are similar to the known `osc()` function, but create more complex shapes. Wherever you use `osc()` in code you could use one of these soft patterns.

## Additional shaders
Load ![lib-softpattern.js](./../../lib/lib-softpattern.js)

## Examples
Find examples made with `lib-softpattern` in this folder: [examples/softpattern](./../../examples/softpattern).

All these filters are of type `src`.

### Blinking grid:
Looks like a grid of colorful square lamps.

`blinking( tiles, scale, speed, phase )`

* `tiles` :: float (default `5.0`)
* `scale` :: float (default `5.0`)
* `speed` :: float (default `0.5`)
* `phase` :: float (default `0.03`) For first attempt use vales near to zero.

```javascript
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/lib-softpattern.js")

blinking(5).add(blinking(10).add(blinking(30))).out(o0)
render(o0)
```

![](./softpattern-blinking.jpg)

### Blobs:
Three blobs are dancing around.

`blobs( speed, tresh, soft )`

* `speed` :: float (default `0.1`)
* `tresh` :: float (default `0.2`)
* `soft` :: float (default `0.05`) For first attempt use positive vales near to zero.

![](./softpattern-blobs.jpg)

### Concentric rings:

Harmonic concentric rings.

`concentric( base, octaves, ampscale, speed )`

* `base` :: float (default `5.0`)
* `octaves` :: float (default `2.0`)
* `ampscale` :: float (default `0.5`)
* `speed` :: float (default `1.0`)

![](./softpattern-concentric.jpg)

### Noise in HSV with phase shifting:

Hue, value and saturation are generated from 3 separate Perlin noise functions. To activate this effect there must be a small phase shift between hue and saturation. This is also true for the difference of the phase between saturation and value.

`phasenoise( base, range, scale, speed, phase )`

* `base` :: float (default `0.0`) Primary color, the color wheel goes from 0.0 to 1.0
* `range` :: float (default `0.1`) Possible deviation from the primary color.
* `scale` :: float (default `5.0`) Scaling for all 3 the underlying noise functions.
* `speed` :: float (default `0.5`) Speed for all 3 the underlying noise functions.
* `phase` :: float (default `0.03`) Phase shift between hue, vale and saturation. For first attempt use vales near to zero.
loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/lib-softpattern.js")

```javascript
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/lib-softpattern.js")

phasenoise(0, 0.15, 5, 0.5, 0.07).out(o0)
render(o0)
```

![](./softpattern-phasenoise.jpg)

### Moving ramp:
Hard to describe with words.

`sdfmove( speed1, speed2, speed3 )`

* `speed1` :: float (default `0.73`)
* `speed2` :: float (default `1.0`)
* `speed3` :: float (default `-0.5`)

![](./softpattern-sdfmove.jpg)

### Smooth sun:
This pattern is dedicated to Sun Ra.
"Light From a Hidden Sun"
published on
"Sunrise in Different Dimensions"

`smoothsun( threshold, border, speed, ampscale )`

* `threshold` :: float (default `0.3`)
* `border` :: float (default `0.2`)
* `speed` :: float (default `1.0`)
* `ampscale` :: float (default `0.5`)

![](./sun_ra-Light_From_a_Hidden_Sun.jpg)
