// licensed with CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/lib-noise.js")

ncontour(0.5, 0.1, 4, 1, 0.5).rotate(() => 0.1*time).out(o0)
ncontour(() => 0.5 + 0.5*Math.sin(0.17*time),
         () => 0.5 + 0.5*Math.sin(0.13*time),
         3,
         () => 6*(0.5 + 0.5*Math.sin(0.13*time)),
         0.9).out(o1)
ncontour(0.5, 0.1, 4, 1).invert().blend(ncontour(0.1, 0.7, 4, 1)).out(o2)
ncontour(0.5, 0.1, 4, 1).invert().modulatePixelate(ncontour(0.1, 0.7, 4, 1)).out(o3)
render()
