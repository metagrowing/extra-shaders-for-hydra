await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/all.js")
window.frame = 0
s0.initCam()
src(s0).out(o2)
render(o3)
src(o3)
    .pysort(0.1,
            // increment the index for the ping pong feedback buffers
            () => {return frame++;},
            // control the direction
            () => {return Math.round(Math.sin(0.3*time));})
    // open the gate to receive new image data
    .blend(o2, () => {return ((frame % 300) == 0) ? 1 : 0;})
    .contrast(1.01)
    .out(o3)
