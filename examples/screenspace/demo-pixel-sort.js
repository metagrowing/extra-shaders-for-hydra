// licensed with CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/all.js")

window.frame = 0
render(o3)
s0.initCam() // initialize a webcam in source buffer s0
src(s0).out(o2)
src(o3)
    .pysort(0.1, () => {return frame++;})
    .blend(o2, () => {return ((frame % 300) == 0) ? 1 : 0.01;})
    .contrast(1.01)
    .out(o3)
 
