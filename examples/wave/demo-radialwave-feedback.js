// licensed with CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/lib-wave.js")

rharmonic(2.3).modulate(rsaw(5)).colorama(0.5).modulateRepeat(o0).out(o0)
tri(1.1).colorama(0.5).colorama(0.5).modulateRotate(o0).out(o1)
render(o1)
