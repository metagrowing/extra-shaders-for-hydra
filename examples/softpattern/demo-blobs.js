// licensed with CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/lib-softpattern.js")

blobs(0.1, 0.5, 0.2).out(o0)
blobs(0.23, 0.5, 0.2).modulate(blobs(0.1, 0.5, 0.2), 1).out(o1)
blobs(0.1, 0.1).invert().kaleid(3).scale(0.3).out(o2)
blobs(0.13, 0.2, 0.2).modulate(blobs(0.21, 0.5, 0.2).modulate(blobs(0.23, 0.9, 0.2), 1), 1).shift().out(o3)
render()

