// licensed with CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/lib-softpattern.js")

blinking(5, 5, 0.5, 0).out(o0)
blinking(5).mult(shape(3).scale(-1.5).modulate(blinking(5))).contrast().out(o1)
blinking(5).add(blinking(10).add(blinking(30))).out(o2)
blinking().scale(0.2).rotate(() => 0.2*time).out(o3)
render()
