// licensed with CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/lib-color.js")
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/lib-softpattern.js")

sdfmove().out(o0)
sdfmove(0.1, 0.2, 0.3).colorama(0.8).blend(sdfmove(-0.1, -0.2, -0.3).rotate()).out(o1)
sdfmove().modulateRepeat(sdfmove(0.13, 0.11, -0.23).rotate(1.57)).out(o2)
sdfmove().rotate(1.57).modulate(noise(1.6, 0.5)).grarose(0.7).out(o3)
render()
