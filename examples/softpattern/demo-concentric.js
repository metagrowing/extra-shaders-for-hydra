// licensed with CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/lib-softpattern.js")

concentric(30).out(o0)
concentric(5,2,0.5).modulate(concentric(20,2,0.5)).out(o1)
concentric(5,4,0.25).color(0.7,0,0).add(concentric(3,2,0.5).scrollY(0.1).color(0.0,0.6,0)).out(o2)
concentric(30,2,0.5,1.31).colorama(0.5).modulateRepeat(concentric(30,2,0.5,0.51),0.1,0.1,0,0.5).out(o3)
render()
