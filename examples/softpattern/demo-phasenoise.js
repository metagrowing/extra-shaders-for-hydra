// licensed with CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/lib-softpattern.js")

phasenoise(0, 0.15, 5, 0.5, 0.07).out(o0)
phasenoise(0.45, 0.3, 25, 1.7, 0.1).diff(o0).out(o1)
phasenoise(0, 0.1, 10).mult(shape(3).scale(1.5).modulate(phasenoise(0, 0.1, 12, 1))).out(o2)
phasenoise(0, 0.1, 10).mult(shape(5).rotate(() => time).scale(2.5).diff(osc())).out(o3)
render()
