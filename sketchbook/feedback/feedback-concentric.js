// licensed with CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/all.js")

concentric(() => {return 12+6*Math.sin(0.051*time);},
           () => {return 0.5+0.5*Math.sin(0.17*time);},
           () => {return 0.5+0.5*Math.sin(0.31*time);})
    .modulateRotate(o3)
    .out(o0)
src(o0).erode().out(o1)
src(o1).blurmore().out(o2)
src(o2).blend(o3).grawave(0.15).out(o3)
render(o3)
