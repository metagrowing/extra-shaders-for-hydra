// licensed with CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
await loadScript("https://cdn.statically.io/gl/metagrowing/extra-shaders-for-hydra/main/lib/all.js")

lissa(() => {return 6*Math.sin(0.051*time);},
      () => {return 3.5+7*Math.sin(-0.17*time);},
      () => {return 3+2*Math.sin(0.31*time);},
      () => {return 2+1.5*Math.sin(0.31*time);})
    .out(o0)
src(o1).modulateScrollY(o0).blend(o0,0.1).out(o1)
src(o1).blur().grarose().saturate(0.5).out(o2)
src(o3).modulateScrollX(o2).blend(o2,0.2).color(0.9,0.6,0.2).contrast(1.1).out(o3)
render(o3)
